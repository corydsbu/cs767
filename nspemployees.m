function [nspEmployeeTitles,nspEmployeeSkills,nspEmployeeNames] = nspemployees()
% Gets data about the employees

%% Titles
chief = 1:2;
attending = 3:6;
resident = 7:10;
primaryCare = 11;
student = 12:14;

titleKeys = {'Chief','Attending','Resident','PrimaryCare','Student'};
titleValues = {chief,attending,resident,primaryCare,student};
nspEmployeeTitles = containers.Map(titleKeys, titleValues);

%% Skills
generalClinic = [3:7,10];
generalClinicAsst = [8:9,12:14];
specialist = 1:2;
fertility = [3:4,6,10,12];
minInvSurgery = 1:2;
minInvSurgeryAsst = [3,6,8:9];
southBuilding = [1:3,5];
ward = 1:7;
frontline = [3:10,12:14];
primaryCare = 11;
freeConsultancy = 3:10;

skillKeys = {'GeneralClinic','GeneralClinicAsst','Specialist', ...
    'Fertility','MinInvSurgery','MinInvSurgeryAsst','SouthBuilding', ...
    'Ward','Frontline','PrimaryCare','FreeConsultancy'};
skillValues = {generalClinic,generalClinicAsst,specialist,fertility, ...
    minInvSurgery,minInvSurgeryAsst,southBuilding,ward,frontline, ...
    primaryCare,freeConsultancy};
nspEmployeeSkills = containers.Map(skillKeys, skillValues);

%% Names
nspEmployeeNames = ["Lv F","Yu D","Li Q","Liu Y",...
    "Wang W","Wang L","Guo L","Hu J","Chen X","Ren L","Yu X","Chen G",...
    "Chen M","Zhang B"];

end
