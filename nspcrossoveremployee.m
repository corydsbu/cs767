function f = nspcrossoveremployee(parents, ~, ~, ~, ~, thisPopulation)
% NSP Crossover by employee
%
% Chooses a random employee in the schedule as the crossover point.
% Employees before that point will come from one parent, employees after
% that point will come from the other parent.
nChildren = length(parents)/2;
f = cell(nChildren, 1);
for i = 1: nChildren
    p1 = thisPopulation{parents(i)};
    p2 = thisPopulation{parents(nChildren + 1 - i)};
    
    nEmployees = size(p1,1);
    crossoverPoint = randi(nEmployees);
    child = p1;
    child(crossoverPoint:nEmployees,:) = p2(crossoverPoint:nEmployees,:);
    
    f{i} = child;
end
    
