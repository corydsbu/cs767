function f = nspmutation(parents, options, nvars, FitnessFcn, state, thisScore, thisPopulation, rate)
% NSP Mutation function
%
% Does random mutations to the specified parents. The longer the algorithm
% goes without improvement, the more mutations are introduced. Mutations
% are introduced faster with higher mutation rate value specified.
f = cell(length(parents), 1);

numMutations = ceil((state.Generation - state.LastImprovement) * rate);
for i = 1: length(parents)
    parent = thisPopulation{parents(i)};
    
    j = 1;
    while j <= numMutations
        employee = randi(size(parent,1));
        day = randi(size(parent,2));
        value = randi(5);
        parent(employee, day) = value;
        j = j + 1;
    end
    f{i} = parent;
end