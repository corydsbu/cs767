function f = nspcrossovermultiday(parents, ~, ~, ~, ~, thisPopulation)
% NSP Multiple crossover by day
%
% Chooses days randomly from one parent or the other to create a new
% schedule.
nChildren = length(parents)/2;
f = cell(nChildren, 1);
for i = 1: nChildren
    p1 = thisPopulation{parents(i)};
    p2 = thisPopulation{parents(nChildren + 1 - i)};
    ps = {p1,p2};
    child = zeros(size(p1));
    nDays = size(child,2);
    for day = 1:nDays
        p = ps(randi(2));
        child(:,day) = p{1}(:,day);
    end
    
    f{i} = child;
end
