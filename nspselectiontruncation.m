function f = nspselectiontruncation(expectation, nParents, options, rate)
% NSP Trucation selection function
%
% Selection method that simply selects the top parents by fitness for the
% next generation. Rate value defines what percentage of population gets
% selected as parents.
f = zeros(1,nParents);

[~,sortedParentIndices] = sort(expectation,'descend');
parentIndexLimit = ceil(length(expectation) * rate);
currentIndex = 1;
for i = 1: nParents
    f(i) = sortedParentIndices(currentIndex);
    if currentIndex < parentIndexLimit
        currentIndex = currentIndex + 1;
    else
        currentIndex = 1;
    end
end