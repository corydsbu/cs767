function nspsaveschedule(employeeNames,schedule,filename)
% NSP Save schedule function
%
% Saves the given schedule as a CSV file
scheduleVal = schedule{1};

% Create empty schedule with room for names and days
finalSchedule = strings(size(scheduleVal)+1);

% Add names
finalSchedule(2:end,1) = employeeNames;

% Add days
daynames = ["Su","M","T","W","R","F","Sa"];
finalSchedule(1,2:end) = daynames(mod(1:length(scheduleVal),7)+1);

% Add shifts
shiftnames = ["","AM","PM","Double","Frontline"];
for i = 1:size(scheduleVal,1)
    for j = 1:size(scheduleVal,2)
        finalSchedule(i+1,j+1) = shiftnames(scheduleVal(i,j));
    end
end

% Save file
fileID = fopen(filename,'w');
fprintf(fileID,...
    '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',...
    transpose(finalSchedule));
fclose(fileID);