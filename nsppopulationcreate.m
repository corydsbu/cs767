function f = nsppopulationcreate(~, ~, options)
% NSP Popuation Creation function
%
% Specific to this problem, creates the initial population of 14 employees x
% 28 days schedules.
f = cell(options.PopulationSize, 1);
for i = 1: options.PopulationSize
    f{i} = randi(5, 14, 28);
end