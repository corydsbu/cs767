function f = nspcrossoverday(parents, ~, ~, ~, ~, thisPopulation)
% NSP Crossover by day
%
% Chooses a random day in the schedule as the crossover point. Days before
% that point will come from one parent, days after that point will come
% from the other parent.
nChildren = length(parents)/2;
f = cell(nChildren, 1);
for i = 1: nChildren
    p1 = thisPopulation{parents(i)};
    p2 = thisPopulation{parents(nChildren + 1 - i)};
    
    nDays = size(p1,2);
    crossoverPoint = randi(nDays);
    child = p1;
    child(:,crossoverPoint:nDays) = p2(:, crossoverPoint:nDays);
    
    f{i} = child;
end
    
