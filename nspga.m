function nspga(populationSize,selectionFcn,crossoverFcn,crossoverFraction,mutationRate,maxGenerations)
%% Main NSP GA function
%
% Parameters
% populationSize: Number of elements in population
%
% selectionFcn: The selection function to use. Some examples:
%   {@selectiontournament, 4} (Tournament selection with 4 players)
%   @selectionroulette (Fitness-proportional selection)
%   {@nspselectiontruncation, 0.5} (Trucation selection with 50% population
%       being retained.
%
% crossoverFcn: The crossover function to use. Options:
%   @nspcrossoverday (Crossover point is a day in the schedule)
%   @nspcrossoveremployee (Crossover point is an employee in the schedule)
%   @nspcrossovermultiday (Multiple crossover points by day)
%
% crossoverFraction: The fraction of schedules that will be considered for
%   crossover
%
% mutationRate: How fast mutations should occur (this is a multiplier)
%
% maxGenerations: Maximum number of generations to run before stopping
%
% e.g.: nspga(100,{@selectiontournament, 4},@nspcrossoverday,.8,1,300)
%
% Resulting schedule is recorded to schedule.csv

%% Initial configuration
% Setup employee data
[nspEmployeeTitles,nspEmployeeSkills,nspEmployeeNames] = nspemployees();

% Setting a plot function to show GA progress
opts = optimoptions(@ga,'PlotFcn',{@gaplotbestf,@gaplotstopping});

%% Population
% Setting population creation function
opts.CreationFcn = @nsppopulationcreate;
opts.PopulationSize = populationSize;

%% Fitness
% Setting fitnessFunction
FitnessFunction = @nspfitness;
numberOfVariables = 1;

%% Selection
% Setting selection function
opts.SelectionFcn = selectionFcn;

%% New generation
% Setting crossover function
opts.CrossoverFcn = crossoverFcn;
opts.CrossoverFraction = crossoverFraction;

% Setting mutation function
opts.MutationFcn = {@nspmutation, mutationRate};

%% Stopping criteria
opts.FitnessLimit = 0;
opts.MaxGenerations = maxGenerations;
opts.MaxStallGenerations = 100;

% Run Fitness function with parallel workers
opts.UseParallel = true;

% Start timer
startTime = tic;

% Run the GA
[Fin,Fval,~,Output] = ga({FitnessFunction,nspEmployeeTitles,nspEmployeeSkills}, ...
    numberOfVariables,[],[],[],[],[],[],[],opts);

% Stop timer
toc(startTime);

fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The best function value found was : %g\n', Fval);

nspsaveschedule(nspEmployeeNames,Fin,"schedule.csv");
