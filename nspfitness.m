function f = nspfitness(element,nspEmployeeTitles,nspEmployeeSkills)
% NSP fitness function
%
% Calculates a fitness score from a given schedule. Lower scores are
% better, a score of 0 would meet all requirements perfectly.
schedule = element{1};
f = 0;

%% Score by day

for day = 1: size(schedule,2)
    dailySchedule = schedule(:,day);
    dayName = getDayName(day);
    
    % --Every day--
    
    % Num Fertility every AM: 1
    count = countSkillsOnDay('Fertility',dailySchedule,getShiftNums("AM"));
    f = increaseScore(f, abs(count - 1)^1.2);
    
    % --Weekdays--
    if ismember(dayName, ["M","T","W","R","F"])
        
        % Num General Clinic M-F AM: 1-2
        generalClinicAmCount = countSkillsOnDay('GeneralClinic',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, min(abs(generalClinicAmCount - 1), abs(generalClinicAmCount - 2))^1.2);

        % Num General Clinic Asst M-F AM: 1-2
        generalClinicAsstAmCount = countSkillsOnDay('GeneralClinicAsst',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(generalClinicAsstAmCount - 1)^1.2);
        
        % Num General Clinic M-F AM + General Clinic Asst M-F AM: 3
        f = increaseScore(f, abs(generalClinicAmCount + generalClinicAsstAmCount - 3)^1.2);
        
        % Num General Clinic M-F PM: 1
        count = countSkillsOnDay('GeneralClinic',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);
        
        % Num General Clinic Asst M-F PM: 1
        count = countSkillsOnDay('GeneralClinicAsst',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);
        
        % Num Fertility M-F PM: 1
        count = countSkillsOnDay('Fertility',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);
        
        % Num South Building M-F AM: 1
        count = countSkillsOnDay('SouthBuilding',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 1)^1.2);
        
        % Num Ward M-F AM: 2
        count = countSkillsOnDay('Ward',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 2)^1.2);

        % Num Ward M-F PM: 1
        count = countSkillsOnDay('Ward',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);

        % Num Frontline M-F: 1
        count = countSkillsOnDay('Frontline',dailySchedule,getShiftNums("Frontline"));
        f = increaseScore(f, (5 * abs(count - 1))^2);

        % Num Primary Care M-F AM: 1
        count = countSkillsOnDay('PrimaryCare',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 1)^1.2);

        % Num Frontline Physician of the day M-F AM: 1
        count = countSkillsOnDay('Frontline',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 1)^1.2);
    end
    
    % Other
    
    % Num General Clinic Sa-Su AM: 1
    if ismember(dayName, ["Sa","Su"])
        count = countSkillsOnDay('GeneralClinic',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 1)^1.2);
    end
    
    % Num Specialist Asst M-R AM: 1
    if ismember(dayName, ["M","T","W","R"])
        count = countSkillsOnDay('Specialist',dailySchedule,getShiftNums("AM"));
        f = increaseScore(f, abs(count - 1)^1.2);
    end

    % Num Min Inv Surgery M,W PM: 1
    if ismember(dayName, ["M","W"])
        count = countSkillsOnDay('MinInvSurgery',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);
    end

    % Num Min Inv Surgery Asst M,W PM: 1
    if ismember(dayName, ["M","W"])
        count = countSkillsOnDay('MinInvSurgeryAsst',dailySchedule,getShiftNums("PM"));
        f = increaseScore(f, abs(count - 1)^1.2);
    end

    % Num Free Consultancy T-F AM + Num Free Consultancy T-F PM: 1
    if ismember(dayName, ["T","W","R","F"])
        count = countSkillsOnDay('FreeConsultancy',dailySchedule,horzcat(getShiftNums("AM"),getShiftNums("PM")));
        f = increaseScore(f, abs(count - 1)^1.2);
    end
    
    % No Frontline shifts on the weekend
    if ismember(dayName, ["Sa","Su"])
        f = increaseScore(f, (5 * sum(dailySchedule == getShiftNums("Frontline")))^2);
    end
end

%% Score by employee

numEmployees = size(schedule,1);
employeeWeeklyShiftCounts = zeros(numEmployees,4);

for employee = 1: numEmployees
    employeeSchedule = schedule(employee,:);

    % Employee should not work Frontline without that skill
    f = increaseScore(f, (5 * sum(employeeSchedule == getShiftNums("Frontline")))^2);

    % Days before Specialist can work again M-R: 2
    currentCount = 0;
    consecutiveCount = 0;
    for day = 1: length(employeeSchedule)
        if ~ismember(employeeSchedule(day), getShiftNums("Off")) && ...
                ismember(getDayName(day), ["M","T","W","R"]) && ...
                ismember(employee, nspEmployeeSkills('Specialist'))
            currentCount = currentCount + 1;
        else
            currentCount = 0;
        end
        
        if currentCount > 1
            consecutiveCount = consecutiveCount + 1;
        end
    end
    f = increaseScore(f, consecutiveCount^1.2);

    % Days before Frontline can work again M-F: 2
    currentCount = 0;
    consecutiveCount = 0;
    for day = 1: length(employeeSchedule)
        if ismember(employeeSchedule(day), getShiftNums("Frontline")) && ...
                ismember(getDayName(day), ["M","T","W","R","F"])
            currentCount = currentCount + 1;
        else
            currentCount = 0;
        end
        
        if currentCount > 1
            consecutiveCount = consecutiveCount + 1;
        end
    end
    f = increaseScore(f, consecutiveCount^1.2);

    % Shift limits
    weeklyShiftCounts = countShiftsPerWeek(employeeSchedule);
    frontlineShiftCount = sum(employeeSchedule == getShiftNums("Frontline"));
    
    if ismember(employee, horzcat(nspEmployeeTitles('Chief'),nspEmployeeTitles('PrimaryCare')))
        % Max Chief/PrimaryCare shifts/week: 5
        f = increaseScore(f, sum(max(weeklyShiftCounts - 5, 0).^1.2));
        
        % Max Chief/PrimaryCare Frontline shifts: 0
        f = increaseScore(f, frontlineShiftCount^1.2);

    elseif ismember(employee, horzcat(nspEmployeeTitles('Attending'),nspEmployeeTitles('Resident')))
        % Max Attending/Resident shifts/week: 6
        f = increaseScore(f, sum(max(weeklyShiftCounts - 6, 0).^1.2));
        
        % Max Attending/Resident Frontline shifts: 3
        f = increaseScore(f, max(frontlineShiftCount - 3, 0)^1.2);

    elseif ismember(employee, nspEmployeeTitles('Student'))
        % Max Student shifts/week: 4
        f = increaseScore(f, sum(max(weeklyShiftCounts - 4, 0).^1.2));

        % Max Student Frontline shifts: 4
        f = increaseScore(f, max(frontlineShiftCount - 4, 0)^1.2);
    end
    
    employeeWeeklyShiftCounts(employee,:) = weeklyShiftCounts;
    
end

% Num Chief shifts/week <= Lowest num other shifts/week
chiefWeeklyShiftCounts = employeeWeeklyShiftCounts(nspEmployeeTitles('Chief'),:);
otherWeeklyShiftCounts = employeeWeeklyShiftCounts(horzcat(...
    nspEmployeeTitles('PrimaryCare'),nspEmployeeTitles('Attending'),...
    nspEmployeeTitles('Resident'),nspEmployeeTitles('Student')),:);

for i = 1: size(chiefWeeklyShiftCounts,1)
    chiefWeeklyShiftCount = chiefWeeklyShiftCounts(i,:);
    for j = 1: size(otherWeeklyShiftCounts,1)
        otherWeeklyShiftCount = otherWeeklyShiftCounts(j,:);
        f = increaseScore(f, sum(max(chiefWeeklyShiftCount - otherWeeklyShiftCount,0).^1.2));
    end
end

% Same num shifts for each title
for title = keys(nspEmployeeTitles)
    weeklyShiftCounts = employeeWeeklyShiftCounts(nspEmployeeTitles(title{1}),:);
    f = increaseScore(f, sum((std(weeklyShiftCounts)+1).^1.2) - 1);
end


%% helper functions

function dayname = getDayName(dayNum)
    daynames = ["Su","M","T","W","R","F","Sa"];
    dayname = daynames(mod(dayNum,7) + 1);
end

function shiftnums = getShiftNums(shift)
    if shift == "Off"
        shiftnums = 1;
    elseif shift == "AM"
        shiftnums = [2,4];
    elseif shift == "PM"
        shiftnums = [3,4];
    elseif shift == "Frontline"
        shiftnums = 5;
    end
end

function skillcount = countSkillsOnDay(skill,dailySchedule,times)
    skillcount = 0;
    
    employees = nspEmployeeSkills(skill);
    for k = employees
        if ismember(dailySchedule(k), times)
            skillcount = skillcount + 1;
        end
    end
end

function weeklyshiftcount = countShiftsPerWeek(employeeSchedule)
    weeklyshiftcount = zeros(4,1);
    
    currentDay = 1;
    for w = 1:4
        currentShiftCount = 0;
        for d = 1:7
            if ismember(employeeSchedule(currentDay),getShiftNums("AM"))
                currentShiftCount = currentShiftCount + 1;
            end
            if ismember(employeeSchedule(currentDay),getShiftNums("PM"))
                currentShiftCount = currentShiftCount + 1;
            end
            if ismember(employeeSchedule(currentDay),getShiftNums("Frontline"))
                currentShiftCount = currentShiftCount + 2;
            end
            
            currentDay = currentDay + 1;
        end
        
        weeklyshiftcount(w) = currentShiftCount;
    end
end

function score = increaseScore(currentScore, addition)
    score = currentScore + addition;
end

end